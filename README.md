# Gradle Dependency Graph Plugin

The Gradle Dependency Plugin generates a visual graph of your Gradle project dependencies. By default it only graphs
sub-projects in a multi-project Gradle build but it can be configured to graph direct external dependencies as well.
Currently it cannot include transient dependencies in the graph.

## How to install it?

The plugin is published to the Gradle Plugin Repository
https://plugins.gradle.org/plugin/com.gitlab.stfs.gradle.dependency-graph-plugin

Instructions on how to include the plugin in your build file can be found there.

Applying the plugin adds the `dependencyGraph` task to your project.

## How to use it?

The plugin adds a single task to your Gradle project:

```
./gradlew dependencyGraph
```

This will generate a graphviz `.dot` file containing the dependency tree into the 
`<project_dir>/build/reports/dependencyGraph/` directory. This file can be viewed or converted into an image in
a number of ways. Below are two examples:

* Using the local graphviz `dot` binary (good for generating static images):
  ```
  dot -Tpng /<project_dir>/build/reports/dependencyGraph/dependenyGraph.dot -o output.png
  ```
  
* Using an online service such as https://www.yworks.com/yed-live/ and uploading the generated `.dot` file (good for 
interactive viewing of the graph).

You can customize the name/location of the `.dot` file and configure the behavior of the plugin (see [Configuration](#configuration) section).

As of version 0.3 of the plugin, it is configured by default to generate a PNG image as well. This can be disabled (see [Configuration](#configuration) section).

## Configuration

The plugin can be configured via a Gradle extension block (default values are shown below):

```
dependencyGraph {
    // Use this block if you want to override configuration for the dependency graph that is generated

    dotOutputFile = project.layout.buildDirectory.file('reports/dependencyGraph/dependencyGraph.dot')

    includeExtDeps = false
    includeExtDepsVersion = true

    extDepDotStyle = "[shape=ellipse, fillcolor=gray80, style=\"rounded,filled\"]"
    prjDepDotStyle = "[shape=box, fillcolor=\"\", style=\"\"]"
    
    generatePng = true
    pngOutputFile = project.layout.buildDirectory.file('reports/dependencyGraph/dependencyGraph.png')
}
```

## Sample output

This is a sample output image of the plugin when run on https://github.com/kissaten/gradle-multi-project-example

![sample output image](img/sample-output.png "")